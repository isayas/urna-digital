Survey.StylesManager.applyTheme("default");
var json = {
  "showProgressBar": "top",
  "goNextPageAutomatic": true,
  "showNavigationButtons": true,
  "showQuestionNumbers": "off",
  "firstPageIsStarted":true,
  "title": "Encuesta Infantil IEQROO",
  "startSurveyText":"Comenzar",
  "pageNextText":"Siguiente",
  "pagePrevText":"Atrás",
  "completeText":"Finalizar",
  "completedHtml":"<h2>El Instituto Electoral de Quintana Roo agradece tu respuesta.</h2><img style='height:70%' src='img/logo_animado.svg' alt='logo ieqroo'/>",
  "pages": [{
    "elements": [{
      "type": "html",
      "name": "intro",
      "title": "Urna Electrónica",
      "html": "<center><img src='img/logo_parlamento.png' alt='logo parlamento' width='100%'/></center> <p>Hola Diputado / Diputada Infantil:<br/>Participa en esta encuesta para conocer datos importantes de tu percepción y entorno.</p>"
    }],
    "name": "intro"
  },{"elements": [{
      "type": "imagepicker",
      "name": "genero",
      "title": "Selecciona si eres:",
      "colCount": 2,
      "choices": [{
        "value": "niño",
        "imageLink": "img/ninio.png"
      }, {
        "value": "niña",
        "imageLink": "img/ninia.png"
      }]
    }],
    "name": "page0"
  }, {
    "elements": [{
      "type": "imagepicker",
      "name": "bullying",
      "title": "¿En tu escuela has sufrido de bullying?",
      "colCount": 2,
      "choices": [{
        "value": "si",
        "imageLink": "img/si.png"
      }, {
        "value": "no",
        "imageLink": "img/no.png"
      }]
    }],
    "name": "page1"
  }, {
    "elements": [{
      "type": "imagepicker",
      "name": "igualdad",
      "title": "¿En tu escuela hay igualdad entre niños y niñas?",
      "colCount": 2,
      "choices": [{
        "value": "si",
        "imageLink": "img/si.png"
      }, {
        "value": "no",
        "imageLink": "img/no.png"
      }]
    }],
    "name": "page2"
  }, {
    "elements": [{
      "type": "imagepicker",
      "name": "sinderechos",
      "title": "¿Consideras que han sido violados tus derechos en tu escuela?",
      "colCount": 2,
      "choices": [{
        "value": "si",
        "imageLink": "img/si.png"
      }, {
        "value": "no",
        "imageLink": "img/no.png"
      }]
    }],
    "name": "page3"
  }, {
    "elements": [{
      "type": "imagepicker",
      "name": "valores",
      "title": "¿En tu casa practican los valores?",
      "colCount": 2,
      "choices": [{
        "value": "si",
        "imageLink": "img/si.png"
      }, {
        "value": "no",
        "imageLink": "img/no.png"
      }]
    }],
    "name": "page4"
  }, {
    "elements": [{
      "type": "imagepicker",
      "name": "tecnologia",
      "title": "¿Crees que el uso de la tecnología sea benéfico para ti?",
      "colCount": 2,
      "choices": [{
        "value": "si",
        "imageLink": "img/si.png"
      }, {
        "value": "no",
        "imageLink": "img/no.png"
      }]
    }],
    "name": "page5"
  }]
};
window.survey = new Survey.Model(json);
/*
survey.onComplete.add(function(result) {
  document.querySelector('#surveyResult').innerHTML = "result: " + JSON.stringify(result.data);
});
*/
$("#surveyElement").Survey({
  model: survey,
  onComplete: sendDataToServer
});

function sendDataToServer(survey) {
    //send Ajax request to your web server.
    //alert("The results are:" + JSON.stringify(survey.data));
    ////Show message about "Saving..." the results
    //options.showDataSaving("Guardando...");//you may pass a text parameter to show your own text
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost/parlamento/responses/?uid=parlamento");
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.onload = xhr.onerror = function () {
        if (xhr.status == 200) {
            //options.showDataSavingSuccess("Resultado guardado"); //you may pass a text parameter to show your own text
            //Or you may clear all messages
            //options.showDataSavingClear();
            setTimeout(function() {
          survey.clear(true, true);
        }, 7000);
        } else {
            //Error
            //options.showDataSavingError("Error al guardar"); //you may pass a text parameter to show your own text
            setTimeout(function() {
          survey.clear(false, true);
        }, 7000);
        }
    };
    xhr.send("data="+JSON.stringify(survey.data));
}


