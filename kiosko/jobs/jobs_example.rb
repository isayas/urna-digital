SCHEDULER.every "4s" do |job|
  send_event("barchart", {
  	type: "bar",
  	header: "Si",
  	labels: ["Bullying","Igualdad","Escuela","Valores","Tecnología"],
  	colorNames: ["lime","aqua","yellow","teal","silver"],
  	datasets: [rand(26), rand(26), rand(26), rand(26), rand(26)]
  })
end
