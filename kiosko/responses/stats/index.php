<?php
/* DEBUG FUNCTION
function shutDownFunction()
{
$error = error_get_last();
// fatal error, E_ERROR === 1
if ($error['type'] === E_ERROR) {
print_r($error);
}
}
register_shutdown_function('shutDownFunction');
 */

function array2table($array, $recursive = false, $null = '&nbsp;')
{
    // Sanity check
    if (empty($array) || !is_array($array)) {
        return false;
    }
    if (!isset($array[0]) || !is_array($array[0])) {
        $array = array($array);
    }
    // Start the table
    $table = "<table>\n";
    // The header
    $table .= "\t<thead><tr>";
    // Take the keys from the first row as the headings
    foreach (array_keys($array[0]) as $heading) {
        $table .= '<th>' . $heading . '</th>';
    }
    $table .= "</tr></thead><tbody>\n";

    // The body
    foreach ($array as $row) {
        $table .= "\t<tr>";
        foreach ($row as $cell) {
            $table .= '<td>';
            // Cast objects
            if (is_object($cell)) {$cell = (array) $cell;}

            if ($recursive === true && is_array($cell) && !empty($cell)) {
                // Recursive mode
                $table .= "\n" . array2table($cell, true, true) . "\n";
            } else {
                $table .= (strlen($cell) > 0) ?
                htmlspecialchars((string) $cell) :
                $null;
            }
            $table .= '</td>';
        }
        $table .= "</tr></tbody>\n";
    }
    $table .= '</table>';
    return $table;
}

$file      = file_get_contents("../parlamento.json");
$tempArray = json_decode($file, true);

$results = [];
foreach ($tempArray["respuestas"] as $val) {
    $tempArray2 = json_decode($val, true);
    foreach ($tempArray2 as $key => $value) {
        //vecho "$key => $value";
        if (!isset($results[$key][$value])) {
            $results[$key][$value] = 1;
        } else {
            $results[$key][$value] += 1;
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>RESULTADOS</title>
  <style>
    body { background-color: #fff; color: #666; text-align: center; font-family: arial, sans-serif; }
    div.dialog {
      width: 25em;
      padding: 0 4em;
      margin: 4em auto 0 auto;
      border: 1px solid #ccc;
      border-right-color: #999;
      border-bottom-color: #999;
      font-weight: bold;
    }
    h1 { font-size: 100%; color: #f00; line-height: 1.5em; }
 table {
  font-family: "Comic Sans MS", cursive, sans-serif;
  border: 4px solid #555555;
  background-color: #C091F4;
  width: 100%;
  text-align: center;
  border-collapse: collapse;
}
table td, table th {
  border: 1px solid #555555;
  padding: 5px 10px;
}
table tbody td {
  font-size: 20px;
  color: #FFFFFF;
}
table thead {
  background: #6D09A4;
}
table thead th {
  font-size: 22px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
}
  </style>
</head>

<body>
  <img width="30%" src="../../assets/images/logo.png">
  <div class="dialog">
    <h1>Terminada la encuesta.</h1>
    <p>Estos son los resultados que tenemos.</p>
    <span>
<?php echo "Respuestas:" . count($tempArray["respuestas"]); ?>
</span>
  </div>
  <br/>
  <?php echo array2table($results, true); ?>
  <img src="../../assets/images/urna.gif">
</body>
</html>

