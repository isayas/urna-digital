<?php
//Permitir el acceso desde cualquier origen
header('Access-Control-Allow-Origin: *');

/*
echo $_GET['uid'];
echo '\n';
echo $_POST['data'];
 */

$encuesta = $_GET['uid'];
$data     = $_POST['data'];

$nombre_archivo = $encuesta . '.json';

// Si el archivo no se ha creado se crea iniciado el arreglo con la estructura
// {"respuestas":[ ]}
if (!file_exists($nombre_archivo)) {
    $gestor = fopen($nombre_archivo, 'a');
    if (fwrite($gestor, '{"respuestas":[ ]}') === false) {
        echo "No se pudo crear el archivo ($nombre_archivo)";
        exit;
    }
}

if (!$gestor = fopen($nombre_archivo, 'a')) {
    echo "No se puede abrir el archivo ($nombre_archivo)";
    exit;
} else {
	$inp       = file_get_contents($nombre_archivo);
    $tempArray = json_decode($inp);
    array_push($tempArray->respuestas, $data);
    file_put_contents($nombre_archivo, json_encode($tempArray));
    echo "Guardado con éxito";
}

fclose($gestor);
